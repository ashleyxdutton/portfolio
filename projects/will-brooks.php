<?php
  $page = "projects";
  $title = "Will Brooks Music | Website Design & Development by Ashley Dutton";
  $description = "Case study about a website I designed and developed for Will Brooks.";
  include "../header.php";
?>

<main class="site-content">
  <section class="case-study">
    <h1 class="acta acta--title">Will Brooks Music</h1>

    <div class="container">

      <div class="case-study__intro">
        <ul>
          <li class="karla karla--bold font--20px font--white">Website Design</li>
          <li class="karla karla--bold font--20px font--white">Website Development</li>
        </ul>

        <div>
          <p class="karla font--18px font--lightgrey">Will approached me to create a simple one-page website for himself as a session musician. I designed and built this website for Will to display videos of his work and testimonials. Will was extremely pleased with the outcome! "I'd highly recommend Ashley to anyone looking for a fast worker who still delivers top quality. I got exactly what I needed in a matter of days, and I couldn't be happier with the finished result. A beautifully clean website, Thank you!" — Will Brooks</p>
          <a class="karla karla--bold font--white link" href="http://willbrooksmusic.co.uk/">View Live Site</a>
        </div>
      </div>

      <div class="case-study__images">
        <img src="/assets/images/projects/willbrooks-mockup.jpg" alt="Will Brooks website open on a MacBook Pro.">
      </div>

    </div>
  </section>
</main>

<?php
  include "../footer.php";
?>
