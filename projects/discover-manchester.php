<?php
  $page = "projects";
  $title = "Discover Manchester | Website Design & Development by Ashley Dutton";
  $description = "Case study about a website I designed and developed in University called Discover Manchester.";
  include "../header.php";
?>

<main class="site-content">
  <section class="case-study">
    <h1 class="acta acta--title">Discover Manchester</h1>

    <div class="container">

      <div class="case-study__intro">
        <ul>
          <li class="karla karla--bold font--20px font--white">Branding</li>
          <li class="karla karla--bold font--20px font--white">Website Design</li>
          <li class="karla karla--bold font--20px font--white">Website Development</li>
        </ul>

        <div>
          <p class="karla font--18px font--lightgrey">Discover Manchester is a project that I worked on during my second year at University. The idea was to create a place for people to easily find great restaurants and bars across Manchester. The design was also featured by a popular Instagram inspiration page - <a class="link font--lightgrey" href="https://www.instagram.com/p/BhZVZbugCey/">welovewebdesign.</a></p>
          <a class="karla karla--bold font--white link" href="http://manchester-food.ashleydutton.co.uk/">View Live Site</a>
        </div>
      </div>

      <div class="case-study__images">
        <img src="/assets/images/projects/discover-mockup.jpg" alt="Discover Manchester website open on a MacBook Pro.">
      </div>

    </div>
  </section>
</main>

<?php
  include "../footer.php";
?>
