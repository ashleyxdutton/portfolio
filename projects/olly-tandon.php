<?php
  $page = "projects";
  $title = "Olly Tandon Music | Website Design & Development by Ashley Dutton";
  $description = "Case study about a website I designed and developed into WordPress for Olly Tandon.";
  include "../header.php";
?>

<main class="site-content">
  <section class="case-study">
    <h1 class="acta acta--title">Olly Tandon Music</h1>

    <div class="container">

      <div class="case-study__intro">
        <ul>
          <li class="karla karla--bold font--20px font--white">Website Design</li>
          <li class="karla karla--bold font--20px font--white">Website Development</li>
          <li class="karla karla--bold font--20px font--white">CMS</li>
        </ul>

        <div>
          <p class="karla font--18px font--lightgrey">Olly approached me to create a web presence to attract potential musicians to book Olly as a session musician. I designed and developed a one-page website, showcasing Olly's promotional video, biography and accolades. The website was also built into Wordpress, giving Olly control over the site to add new testimonials and make changes on the fly.</p>
          <a class="karla karla--bold font--white link" href="http://ollytandonmusic.co.uk/">View Live Site</a>
        </div>
      </div>

      <div class="case-study__images">
        <img src="/assets/images/projects/ollytandon-mockup.jpg" alt="Olly Tandon Music website open on a MacBook Pro.">
      </div>

    </div>
  </section>
</main>

<?php
  include "../footer.php";
?>
