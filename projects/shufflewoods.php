<?php
  $page = "projects";
  $title = "Shufflewoods | Website Design & Development by Ashley Dutton";
  $description = "Case study about an eCommerce website I designed and developed for Shufflewoods.";
  include "../header.php";
?>

<main class="site-content">
  <section class="case-study">
    <h1 class="acta acta--title">Shufflewoods</h1>

    <div class="container">

      <div class="case-study__intro">
        <ul>
          <li class="karla karla--bold font--20px font--white">Website Design</li>
          <li class="karla karla--bold font--20px font--white">Website Development</li>
          <li class="karla karla--bold font--20px font--white">eCommerce</li>
        </ul>

        <div>
          <p class="karla font--18px font--lightgrey">Shufflewoods is a small, special home-grown design business specialising in wooden based products for all occasions. Shufflewoods wanted a clean website to branch out and sell their products online. I designed and developed the website, using Ecwid for easy management of their products.</p>
          <a class="karla karla--bold font--white link" href="http://shufflewoods.co.uk/">View Live Site</a>
        </div>
      </div>

      <div class="case-study__images">
        <img src="/assets/images/projects/shufflewoods-mockup.jpg" alt="Shufflewoods eCommerce website open on a MacBook Pro.">
      </div>

    </div>
  </section>
</main>

<?php
  include "../footer.php";
?>
