<?php
  $page = "projects";
  $title = "Brookdale Garage | Website Design & Development by Ashley Dutton";
  $description = "Case study about a website I designed and developed for Brookdale Garage.";
  include "../header.php";
?>

<main class="site-content">
  <section class="case-study">
    <h1 class="acta acta--title">Brookdale Garage</h1>

    <div class="container">

      <div class="case-study__intro">
        <ul>
          <li class="karla karla--bold font--20px font--white">Website Design</li>
          <li class="karla karla--bold font--20px font--white">Website Development</li>
          <li class="karla karla--bold font--20px font--white">Social Media</li>
        </ul>

        <div>
          <p class="karla font--18px font--lightgrey">Having been a well-established vehicle garage since 1972, Brookdale Garage wanted to modernise their business by gaining a web presence. I designed and developed a modern website for Brookdale Garage and also created a Facebook page as a way for them to communicate with current and potential clients.</p>
          <a class="karla karla--bold font--white link" href="http://brookdale-garage.co.uk/">View Live Site</a>
        </div>
      </div>

      <div class="case-study__images">
        <img src="/assets/images/projects/brookdale-mockup.jpg" alt="Brookdale Garage website open on a MacBook Pro.">
      </div>

    </div>
  </section>
</main>

<?php
  include "../footer.php";
?>
