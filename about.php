<?php
  $page = "about";
  $title = "About Me | Website Design & Development by Ashley Dutton";
  $description = "Learn more about me, what I do and my previous experience working on the web.";
  include "header.php";
?>

<main class="site-content" style="z-index:99;">
  <section class="about">
    <h1 class="acta acta--title">About Me</h1>

    <div class="container">
      <div class="about__text">
        <p class="karla karla--bold font--30px font--white">Hi there, I'm Ashley, a front-end developer and digital designer based in Manchester, UK. I love working with people to produce attractive and functional digital experiences.</p>
        <p class="karla font--20px font--lightgrey">My skills are focused around front-end development, user interface design, branding and WordPress. I am passionate about both development and design and have experience working with a range of clients. I also work as a front-end developer at digital agency Supremo. If you would like to work with me then <a href="/contact">let’s chat!</a></p>

        <ul>
          <li class="acta font--white">Skills</li>
          <li class="karla font--lightgrey">Branding</li>
          <li class="karla font--lightgrey">Website Design</li>
          <li class="karla font--lightgrey">Website Development</li>
          <li class="karla font--lightgrey">CMS</li>
        </ul>

        <ul>
          <li class="acta font--white">Links</li>
          <li class="karla"><a class="font--lightgrey" href="mailto:hello@ashleydutton.co.uk">Email</a></li>
          <li class="karla"><a class="font--lightgrey" href="https://twitter.com/ashleyxdutton" target="_blanc">Twitter</a></li>
          <li class="karla"><a class="font--lightgrey" href="https://dribbble.com/ashleyxdutton" target="_blanc">Dribbble</a></li>
          <li class="karla"><a class="font--lightgrey" href="https://gitlab.com/ashleyxdutton" target="_blanc">Gitlab</a></li>
          <li class="karla"><a class="font--lightgrey" href="/cv">CV</a></li>
        </ul>
      </div>

      <div class="about__image"></div>

    </div>
  </section>
</main>

<?php
  include "footer.php";
?>
