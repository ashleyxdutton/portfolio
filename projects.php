<?php
  $page = "projects";
  $title = "Projects | Website Design & Development by Ashley Dutton";
  $description = "View some of the website design and development projects that I have been working on lately.";
  include "header.php";
?>

<main class="site-content">
  <section class="projects">
    <h1 class="acta acta--title">Projects</h1>

    <div class="container">
      <ul class="projects__list">

        <li class="projects__item" data-tilt>
          <a class="projects__link projects__link--discover" href="/projects/discover-manchester">
            <div class="projects__overlay"></div>
            <div class="projects__text">
              <h2 class="acta font--white font--30px">Discover Manchester</h2>
              <p class="karla font--white font--14px">Website Design & Development</p>
            </div>
          </a>
        </li>

        <li class="projects__item">
          <a class="projects__link projects__link--ollytandon" href="/projects/olly-tandon">
            <div class="projects__overlay"></div>
            <div class="projects__text">
              <h2 class="acta font--white font--30px">Olly Tandon Music</h2>
              <p class="karla font--white font--14px">Website Design & Development</p>
            </div>
          </a>
        </li>

        <li class="projects__item" data-tilt>
          <a class="projects__link projects__link--willbrooks" href="/projects/will-brooks">
            <div class="projects__overlay"></div>
            <div class="projects__text">
              <h2 class="acta font--white font--30px">Will Brooks Music</h2>
              <p class="karla font--white font--14px">Website Design & Development</p>
            </div>
          </a>
        </li>

        <!-- <li class="projects__item">
          <a class="projects__link projects__link--brookdale" href="/projects/brookdale-garage.php">
            <div class="projects__overlay"></div>
            <div class="projects__text">
              <h2 class="acta font--white font--30px">Brookdale Garage</h2>
              <p class="karla font--white font--14px">Website Design & Development</p>
            </div>
          </a>
        </li>

        <li class="projects__item">
          <a class="projects__link projects__link--shufflewoods" href="/projects/shufflewoods.php">
            <div class="projects__overlay"></div>
            <div class="projects__text">
              <h2 class="acta font--white font--30px">Shufflewoods</h2>
              <p class="karla font--white font--14px">Website Design & Development</p>
            </div>
          </a>
        </li> -->
      </ul>
    </div>
  </section>
</main>

<?php
  include "footer.php";
?>
