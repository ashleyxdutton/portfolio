<?php
  $page = "blog";
  $title = "Performance and Optimisation | Website Design & Development by Ashley Dutton";
  $description = "Blog post talking about the tools and techniques used to design and develop my portfolio.";
  include "../header.php";
?>

<main class="site-content" style="z-index:99;">
  <section class="blog-post">
    <h1 class="acta acta--title">Performance and Optimisation</h1>

    <div class="blog-post__meta">
      <img src="/assets/images/user.jpg" alt="Portrait image of myself.">
      <div>
        <p class="karla font--lightgrey">8th November 2018</p>
        <p class="karla font--lightgrey">4 min read time</p>
      </div>
    </div>

    <div class="blog-post__content">
      <p>Having a nicely designed, fancy website is all good and well, but if the website performs poorly the website is going to fail and a lot of users will leave without hesitation. The importance of web performance is crucial and a quick look on WPOStats.com will show that minor performance improvements can produce a massive impact. For example, Netflix saw a 43% decrease in their bandwidth bill after turning on GZip (WPOStats, 2008).</p>
      <img src="/assets/images/blog/performance.jpg" alt="">

      <h2>Code</h2>
      <p>When writing the code for my portfolio, I tried to stay short and sweet. I ensured that I didn't end up with bloated code that would impact my performance. I also reduced my HTTP requests to a minimum as the more files the browser needs to load, the longer the loading times (Comack, 2016). Another performance enhancement I made was minifying my CSS. As I am using SASS compiled with Grunt, my CSS is automatically minified. Throughout developing my website, I tested each page using the W3C Markup Validation Service which checks the validity of my markup to ensure there are no errors which could potentially affect the performance.</p>

      <h2>Optimisation</h2>
      <p>One of the most impactful performance enhancements that I made to my website was through using the htaccess file. Htaccess is a configuration file used by Apache-based web servers that controls the directory (Kohmin, 2014). This means using the htaccess file, I can make adjustments on how the server handles my website. One of the things Google PageSpeed will advise is to 'Leverage browser caching' and this is done using the htaccess file. Leveraging browser caching will reduce the load on my web server for returning visitors and create a much faster load time for my users my storing my files in the user's cache. Another important performance enhancement I made using the htaccess file in enabling Gzip compression on my server. Gzip compresses my websites files before sending them over the user's browser, reducing sizes of my pages by up to 70% (GTMetrix, no date).</p>
      <blockquote>
        "Gzip compresses your webpages and style sheets before sending them over to the browser. This drastically reduces transfer time since the files are much smaller."<br>- GTMetrix
      </blockquote>
      <p>I also optimised all of my images to ensure they were at the smallest file size for optimum delivery and at the best possible quality. I used Adobe Photoshop to resize my images to a suitable size and optimise the size and then ran them through an application called ImageOptim which removes bloated metadata and compresses images without destroying the quality.</p>

      <h2>SEO Integration</h2>
      <p>In order to integrate good search engine optimisation, I ran through a series of checks. Starting with a really simple yet super effective one, page titles and descriptions. I made sure that each page had a relevant and sufficient page title and a description containing plenty of valuable keywords. All of the performance enhancements mentioned above will also contribute to improved search engine optimisation.</p>

      <p>See the results below from Google PageSpeed Insights and WebPageTest to see just how effective these performance enhancements have been to my website.</p>
      <img src="/assets/images/blog/performance/google-pagespeed.jpg" alt="Screeshot of Google PageSpeed Insights results after checking my websites performance.">
      <img src="/assets/images/blog/performance/webpagetest.jpg" alt="Screeshot of WebPageTest results after checking my websites performance.">

      <div class="blog-post__bibliography">
        <h2>Bibliography</h2>
        <p>Comack. (2016) <em>How to reduce HTTP requests to speed up your WordPress site</em>. [Online] [Accessed on 8th November 2018] <a href="https://yoast.com/reduce-http-requests-wordpress/">https://yoast.com/reduce-http-requests-wordpress/</a></p>
        <p>GTMetrix. (no date) <em>PageSpeed: Enable gzip compression</em>. [Online] [Accessed on 8th November 2018] <a href="https://gtmetrix.com/enable-gzip-compression.html">https://gtmetrix.com/enable-gzip-compression.html</a></p>
        <p>Kohmin. (2014) <em>All hands on .htaccess</em>. [Online] [Accessed on 8th November 2018] <a href="https://medium.com/@TDiawo/all-hands-on-htaccess-7afcbb1fd4e9">https://medium.com/@TDiawo/all-hands-on-htaccess-7afcbb1fd4e9</a></p>
        <p>WPOStats. (2008) <em>Netflix saw a 43% decrease in their bandwidth bill after turning on GZip.</em>. [Online] [Accessed on 8th November 2018] <a href="https://wpostats.com/2015/11/04/netflix-bandwidth.html">https://wpostats.com/2015/11/04/netflix-bandwidth.html</a></p>
      </div>

    </div> <!-- post content end -->

    <div class="blog-post__view-more">
      <h2 class="acta acta--title font--25px">View More Posts</h2>
      <ul class="blog__list">
        <li class="blog__post">
          <a href="/blog/the-industry">
            <div class="blog__image" style="background-image:url('/assets/images/blog/the-industry.jpg')"></div>
            <div class="blog__text">
              <p class="karla font--14px font--lightgrey">15th October 2018</p>
              <h2 class="acta font--30px font--white">The UK's Digital Industry</h2>
            </div>
          </a>
        </li>

        <li class="blog__post">
          <a href="/blog/building-my-portfolio">
            <div class="blog__image" style="background-image:url('/assets/images/blog/portfolio.jpg')"></div>
            <div class="blog__text">
              <p class="karla font--14px font--lightgrey">30th October 2018</p>
              <h2 class="acta font--30px font--white">Building My Portfolio</h2>
            </div>
          </a>
        </li>
      </ul>
    </div>

  </section>
</main>

<?php include "../footer.php";?>
