<?php
  $page = "blog";
  $title = "Introduction to GreenSock | Website Design & Development by Ashley Dutton";
  $description = "Blog post talking about the tools and techniques used to design and develop my portfolio.";
  include "../header.php";
?>

<main class="site-content" style="z-index:99;">
  <section class="blog-post">
    <h1 class="acta acta--title">Introduction to GreenSock</h1>

    <div class="blog-post__meta">
      <img src="/assets/images/user.jpg" alt="Portrait image of myself.">
      <div>
        <p class="karla font--lightgrey">26th March 2019</p>
        <p class="karla font--lightgrey">4 min read time</p>
      </div>
    </div>

    <div class="blog-post__content">
      <h2>Aim & Objectives</h2>
      <h3>Aim</h3>
      <p>To create an animated microsite highlighting environmental issues using GreenSock.</p>
      <h3>Objectives</h3>
      <ul>
        <li>Research current environmental issues to establish content.</li>
        <li>Design a prototype of how the site will look.</li>
        <li>Using Adobe XD’s new “auto-animate” feature, experiment with various animations.</li>
        <li>Develop the site using HTML, CSS (using CSS Grid), JavaScript and GreenSock.</li>
      </ul>

      <img src="/assets/images/blog/greensock.jpg" alt="Laptop with SaveOurPlanet project open.">

      <h2>Definition</h2>
      <p>So what is GreenSock? <a href="https://greensock.com/" target="_blank">GreenSock Animation Platform (GSAP)</a> is a JavaScript library used by front-end developers to create animations on webpages. GSAP is a powerful animation tool and is used by major companies such as Lexus, Adobe and Nike. The library allows for precise control over more complex timeline-based animations, something that cannot be achieved with CSS keyframes. <a href="https://ihatetomatoes.net/web-animation-trends/" target="_blank">Petr Tichy from Ihatetomatoes</a> deconstructed 31 site-of-the-day websites and revealed that 67% of those sites were using GreenSock as its animation platform, proving the increasing popularity of the animation library.</p>
      <p>Prior to starting this project, I had never used GSAP or any JavaScript animation library before. Any animations I have used before have been basic CSS transitions or a CSS keyframe animation. I do have a basic understanding of JavaScript which helped a lot with understanding GSAP.</p>

      <h2>Set Up</h2>
      <p>To create this project, HTML, CSS (SCSS), BEM Methodology, JavaScript, GreenSock and a little bit of jQuery was used. Adobe XD was also used to design the project and explore some potential animations.</p>

      <h2>Location</h2>
      <p>To view this project live, visit: <a href="http://saveourplanet.ashleydutton.co.uk" target="_blank">SaveOurPlanet</a></p>
      <p>To view the source code of this project, see my <a href="https://gitlab.com/ashleyxdutton/saveourplanet" target="_blank">Gitlab project.</a></p>

      <h2>How</h2>
      <p>Once I was happy with the designs and had a rough idea of some animations that I wanted, I started to code the markup and styling. I created the main page and the introduction/loading screen first and then started to think about adding some animation. With no prior experience using GSAP, I found <a href="https://greensock.com/docs" target="_blank">GreenSock's documentation</a> to be extremely useful. The loading page was the first to receive some GSAP treatment as I had some solid ideas in my head of how this was going to work. </p>

      <p>GSAP comes with four core tools: TweenLite, TweenMax, TimelineLite, TimelineMax. I first had to understand what do these tools do and how do I use them. I found that the GSAP documentation explains the usage for these well. TweenLite is simply the tool for the animation itself, with TweenMax being the same but with extended functionality. TimelineLite is used to sequence the animations (tweens) into a, well, a timeline, with TimelineMax offering some extended functionality.</p>

      <p>I started by creating all the variables for the things that I wanted to animate and initialised a new TimelineMax instance.</p>

      <img src="/assets/images/blog/gsap/gsap-new-timeline.jpg" alt="Screenshot of code creating a new timeline.">

      <p>Not a bad start, but now I actually had to figure out how to make the animations happen. For the first animation of fading the map in, I used TweenMax's fromTo() method to animate the map from opacity 0 to opacity 1.</p>

      <img src="/assets/images/blog/gsap/gsap-map-animation.jpg" alt="Screenshot of code to animate map.">

      <p>This line of code means that firstly, add a new animation to the timeline (tl.add), then create an instance of TweenMax.fromTo(). Inside the brackets defines what is being animated, in this case, the variable 'map', for 2 seconds in length to animate from opacity:0 to opacity:1.</p>

      <p>To achieve the letter rippling effect, I created a small function using jQuery and a regular expression to select all the characters inside the tag and wrap them in all in their own span tag to be animated by GSAP.</p>

      <img src="/assets/images/blog/gsap/gsap-regex.jpg" alt="Screenshot of regular expression function.">

      <p>After searching around, I found that TweenMax had a .staggerFromTo() function. This allowed me to target all of the individual spans and apply the same animation whilst setting an increment between each one.</p>

      <img src="/assets/images/blog/gsap/gsap-stagger.jpg" alt="Screenshot demonstrating GreenSock's stagger function.">

      <p>Here I encountered an issue, for a few milliseconds on the initial page load, all of the characters and the map would display together before running the animation. This was due to the script being loaded after the DOM. To resolve this issue, I set the visibility of the letters and map to hidden in the CSS which will then be changed to visible when the script loads. The rest of the loading page animation was completed using TweenLite.to() as the initial values were already set using CSS. The loading animation ran successfully and how I wanted, however, I encountered another issue. Despite not being visible, the loader section was still overlapping the main content and prevented any of the links being clicked. I found my solution on this <a href="https://greensock.com/forums/topic/14588-how-to-set-display-to-none-after-transition-is-complete" target="_blank">GreenSock forum</a>, as someone else had a similar issue. Here I learned about the autoAlpha value which allowed me to set the whole loaders visibility to hidden once the timeline had reached the end.</p>

      <p>I was really pleased with how the animation turned out, however, the duration was quite long at roughly 16 seconds. I discovered on the <a href="https://ihatetomatoes.net/wp-content/uploads/2016/07/GreenSock-Cheatsheet-4.pdf" target="_blank">'GreenSock Cheat Sheet'</a> that the timeline playback could be controlled and played from a certain point in the timeline. I added in a basic skip button with an onclick event to skip the animation to 15 seconds.</p>

      <img src="/assets/images/blog/gsap/gsap-skip.jpg" alt="Screenshot showing code for skip button functionality.">

      <h2>Evaluation</h2>
      <p>With no prior experience with GreenSock, it took a long time to develop the project as I was constantly learning along the way. Having now created a project using GreenSock, it is clear why it is making waves in the development community as a go-to library for animation. GreenSock is extremely useful and with the right resources, is relatively easy to learn. In a few lines of code, powerful animations can be achieved which breathes life into a static website, making it stand out among other sites.</p>
      <p>One thing I would like to improve on this project is the performance of the animations. The performance of the website itself is fine, scoring 100 (desktop) on Google PageSpeed Insights. However, the animations are not as seamless I would like as they sometimes lag a little bit. This is something I am going to continuously look to improve.</p>
      <p>I am pleased with what I have achieved with this project and I now feel confident using GreenSock to add animation to my future projects. I encountered plenty of obstacles whilst developing this project but managed to successfully overcome them.</p>

      <a href="http://saveourplanet.ashleydutton.co.uk" target="_blank">View SaveOurPlanet</a>

      <div class="blog-post__bibliography">
        <h2>Bibliography</h2>
        <p>GreenSock (no date) <em>Getting started with DOCS.</em> [Online] [Accessed 12th March 2019] <a href="https://greensock.com/docs">https://greensock.com/docs</a></p>
        <p>Kramer, N. (2018) <em>The Beginner’s Guide to the GreenSock Animation Platform.</em> [Online] [Accessed 12th March 2019] <a href="https://medium.freecodecamp.org/the-beginners-guide-to-the-greensock-animation-platform-7dc9fd9eb826">https://medium.freecodecamp.org/the-beginners-guide-to-the-greensock-animation-platform-7dc9fd9eb826</a></p>
        <p>Tichy, P. (2016) <em>Web Animation Trends: 31 Top Websites Deconstructed.</em> [Online] [Accessed 12th March 2019] <a href="https://ihatetomatoes.net/web-animation-trends/">https://ihatetomatoes.net/web-animation-trends/</a></p>
      </div>

    </div> <!-- post content end -->

    <div class="blog-post__view-more">
      <h2 class="acta acta--title font--25px">View More Posts</h2>
      <ul class="blog__list">
        <li class="blog__post">
          <a href="/blog/the-industry">
            <div class="blog__image" style="background-image:url('/assets/images/blog/the-industry.jpg')"></div>
            <div class="blog__text">
              <p class="karla font--14px font--lightgrey">15th October 2018</p>
              <h2 class="acta font--30px font--white">The UK's Digital Industry</h2>
            </div>
          </a>
        </li>

        <li class="blog__post">
          <a href="/blog/building-my-portfolio">
            <div class="blog__image" style="background-image:url('/assets/images/blog/portfolio.jpg')"></div>
            <div class="blog__text">
              <p class="karla font--14px font--lightgrey">30th October 2018</p>
              <h2 class="acta font--30px font--white">Building My Portfolio</h2>
            </div>
          </a>
        </li>
      </ul>
    </div>

  </section>
</main>

<?php include "../footer.php";?>
