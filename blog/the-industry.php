<?php
  $page = "blog";
  $title = "The UK's Digital Industry | Website Design & Development by Ashley Dutton";
  $description = "Blog post talking about the digital industry in the UK.";
  include "../header.php";
?>

<main class="site-content" style="z-index:99;">
  <section class="blog-post">
    <h1 class="acta acta--title">The UK's Digital Industry</h1>

    <div class="blog-post__meta">
      <img src="/assets/images/user.jpg" alt="Portrait image of myself.">
      <div>
        <p class="karla font--lightgrey">15th October 2018</p>
        <p class="karla font--lightgrey">6 min read time</p>
      </div>
    </div>

    <div class="blog-post__content">
      <p>In the UK, the digital technology ecosystem has experienced a steady grown for the past few years, with employment and company turnover both increasing each year. According to the Tech Nation Report 2018, From 2014 to 2017, the digital tech sector employment rose 13.2% and the turnover of digital tech companies grew by 4.5% from 2016 to 2017 compared with a 1.7% growth in UK GDP. These are interesting statistics and shows exactly how the UK's tech industry is growing substantially.</p>
      <img src="/assets/images/blog/the-industry.jpg" alt="Landscape photograph of Manchester City Centre.">
      <p>After reading both the Tech Nation Report 2018 and Manchester Digital's Skill Audit Report 2018, I was pleasantly surprised by what I found out. As a student aspiring to work within the digital tech industry in the UK, it was exciting to discover just how successful the UK is within the global tech industry.</p>

      <h2>Developers in High Demand</h2>
      <p>One of the most interesting things that I discovered from these two documents was the demand for developers. From the Manchester Digital Skills Audit, 47% of businesses were recruiting for developers, however, 49% of businesses reported that the developer role was the most difficult role to fill. A wake-up call for me was the highest demanded programming language - JavaScript.</p>
      <p>Given the power of JavaScript, this didn't come at much of a surprise to me, but it certainly made me think about my JavaScript skills. A large 81% of developers surveyed either agreed or mostly agreed that JavaScript is moving in the right direction, a slight increase from 78% in the 2016 report (State of JavaScript, 2017). This goes to show how important JavaScript is in the tech industry and has made it clear that I really need to hone my JavaScript skills in order to gain success in the industry.</p>

      <h2>Industry Strengths</h2>
      <p>In the top three reported strengths of the Manchester tech industry, a helpful tech community and proximity to a University was noted. Studying in Manchester, I feel fortunate to be surrounded by such an involved industry. With so many tech meet-ups, such as CodeUp Manchester, BLAB, McrFRED, HacketNest and many more, it's clear that Manchester is a thriving city for digital activity, which puts me in a great position trying to break into the industry.</p>
      <p>On the contrary, the three reported challenges of the Manchester tech industry were access to funding, access to talent and lack of tech training. I was surprised by the access to talent given that Manchester is surrounded by Universities and other learning institutions, such as Northcoders. This made me think about ways how I can get myself out there and noticed by employers.</p>

      <h2>UK Digital Suburbs</h2>
      <p>Having read the report I was pleasantly surprised to find out that the digital industry thrives in suburbs as well as cities. In fact, Newbury's digital density is the most tech specialised local economy at 15.5, compared to London's 0.92. Having assumed that a digital career revolved around cities, I was happy to find that the tech industry is thriving around the whole UK, too.</p>

      <h2>Brexit</h2>
      <p>With Brexit steadily approaching, a lot of uncertainty is being put on UK tech firms about the future. From the Manchester Digital Skills Audit Report, 45% said their business had felt effects from the uncertainty around Brexit. Digital companies in London at the most connected in Europe, with 33% of customers being based outside of the UK. It, therefore, comes as no surprise that UK tech firms are feeling uncertainty about the future. As Brexit takes place in 2019 around my time of graduation, it is possible that the potential negative effects of Brexit will start to take place as I am trying to gain a career in the UK industry.</p>

      <h2>Conclusion</h2>
      <p>Having read these documents, I am both pleased and excited about my future within the digital industry. It is clear that I need to enhance both my soft skills and JavaScript ability before I pursue a career in the digital industry.</p>

      <div class="blog-post__bibliography">
        <h2>Bibliography</h2>
        <p>Manchester Digital. (2018) <em>Manchester Digital Skills Audit Report 2018</em>. [Online] [Accessed on 15th October 2018] <a href="https://www.manchesterdigital.com/sites/default/files/Conference%20Day%20Audit_v3_web.pdf">https://www.manchesterdigital.com/sites/default/files/Conference%20Day%20Audit_v3_web.pdf</a></p>
        <p>StateOfJS. (2017) <em>The State Of JavaScript 2017</em>. [Online] [Accessed on 15th October 2018] <a href="https://2017.stateofjs.com/2017">https://2017.stateofjs.com/2017</a></p>
        <p>Tech Nation. (2018) <em>Tech Nation Report 2018</em>. [Online] [Accessed on 15th October 2018] <a href="https://technation.io/insights/report-2018/">https://technation.io/insights/report-2018/</a></p>
      </div>

    </div> <!-- post content end -->

    <div class="blog-post__view-more">
      <h2 class="acta acta--title font--25px">View More Posts</h2>
      <ul class="blog__list">

        <li class="blog__post">
          <a href="/blog/building-my-portfolio">
            <div class="blog__image" style="background-image:url('/assets/images/blog/portfolio.jpg')"></div>
            <div class="blog__text">
              <p class="karla font--14px font--lightgrey">30th October 2018</p>
              <h2 class="acta font--30px font--white">Building My Portfolio</h2>
            </div>
          </a>
        </li>

        <li class="blog__post">
          <a href="/blog/performance-and-optimisation">
            <div class="blog__image" style="background-image:url('/assets/images/blog/performance.jpg')"></div>
            <div class="blog__text">
              <p class="karla font--14px font--lightgrey">8th November 2018</p>
              <h2 class="acta font--30px font--white">Performance and Optimisation</h2>
            </div>
          </a>
        </li>
      </ul>
    </div>

  </section>
</main>

<?php include "../footer.php";?>
