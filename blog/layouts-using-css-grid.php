<?php
  $page = "blog";
  $title = "Layouts using CSS Grid | Website Design & Development by Ashley Dutton";
  $description = "Blog post talking about the tools and techniques used to design and develop my portfolio.";
  include "../header.php";
?>

<main class="site-content" style="z-index:99;">
  <section class="blog-post">
    <h1 class="acta acta--title">Layouts using CSS Grid</h1>

    <div class="blog-post__meta">
      <img src="/assets/images/user.jpg" alt="Portrait image of myself.">
      <div>
        <p class="karla font--lightgrey">26th March 2019</p>
        <p class="karla font--lightgrey">5 min read time</p>
      </div>
    </div>

    <div class="blog-post__content">
      <p>CSS Grid is a relatively new CSS module allowing developers to create complex website layouts in a two-dimensional grid. CSS Grid is now being widely used and supported in most modern browsers. As I have not used CSS Grid before, I decided that this project was an ideal opportunity to experiment with Grid and learn how it can be used for layouts. </p>

      <img src="/assets/images/blog/css-grid.jpg" alt="">

      <h2>Aim & Objectives</h2>
      <h3>Aim</h3>
      <p>To use CSS Grid to create a responsive layout for the internal pages of SaveOurPlanet.</p>
      <h3>Objectives</h3>
      <ul>
        <li>Research current environmental issues to establish content.</li>
        <li>Design a prototype of how the site will look.</li>
        <li>Using Adobe XD’s new “auto-animate” feature, experiment with various animations.</li>
        <li>Develop the site using HTML, CSS (using CSS Grid), JavaScript and GreenSock.</li>
      </ul>

      <h2>Definition</h2>
      <p>CSS Grid is a relatively new layout module implemented into CSS. Unlike other layout systems in CSS, such as Flexbox, CSS Grid is two dimensional, meaning it can handle both columns and rows. This makes CSS Grid a powerful tool to create modern website layouts. CSS Grid provides a way for developers to create more complex layouts without having to rely on hacks. As Grid has been around for a while now, browser support is looking good. Grid is now fully supported in all major browsers such as Chrome, Firefox, Safari and Edge, as well as having partial support in IE10 and 11.</p>

      <img src="/assets/images/blog/grid/grid-browsers.jpg" alt="">

      <h2>Set Up</h2>
      <p>To create this project, HTML, CSS (SCSS), BEM Methodology, JavaScript, GreenSock and a little bit of jQuery was used. To version control this project, I used Gitlab. I used Firefox Developer Edition as this comes equipped with a grid viewer which was useful when developing this project, as I could visually see the grid and understand the effects that certain declarations were having on the layout.</p>

      <h2>Location</h2>
      <p>To view this project live, visit: <a href="http://saveourplanet.ashleydutton.co.uk" target="_blank">SaveOurPlanet</a></p>
      <p>To view the source code of this project, see my <a href="https://gitlab.com/ashleyxdutton/saveourplanet" target="_blank">Gitlab project.</a></p>

      <h2>How</h2>
      <p>For this project, I intended on using CSS Grid on the internal pages that were designed using a grid layout. Before attempting to use CSS Grid, I developed the loading screen and main screen first using other methods such as Flexbox, as this did not require a grid. Once this had been completed, I began working on the internal pages. With no prior experience using CSS Grid, I first set out to learn all the terminology. I found <a href="https://css-tricks.com/snippets/css/complete-guide-grid/" target="_blank">'A Complete Guide to Grid' on CSS Tricks</a> a very useful resource for this as the terminology is nicely broken up into understandable segments.</p>

      <p>Establishing CSS Grid is simple, by putting 'display: grid;' on the container, the grid format will be applied. Next, I looked at how to recreate the grid I used in Adobe XD in CSS. After reading the CSS Tricks article, I learned that grid column and rows are defined using 'grid-template-columns' and 'grid-template-rows'. I had 11 columns in my design with a maximum width of 1100px. To do this, I used 'grid-template-columns: repeat(11, 1fr);', meaning there will be 11 columns at 1 fraction each so that all my columns will be of equal size. The content that I had was two blocks of text and two images. To position these within the grid, I used 'grid-column' and 'grid-row' on the elements. The image below shows the code that I used to position these elements within the grid.</p>

      <img src="/assets/images/blog/grid/grid-positioning.jpg" alt="">

      <p>For example, '&__column-one-text' is set to start at column 2 and span over 4 columns and start at row 3.</p>

      <p>Initially, I used fixed amounts for my rows and this presented an issue when positioning the child elements. When making the browser smaller to check responsiveness, the text would begin to overlap the image as the text block dropped on to more lines. To overcome this, I changed the row declaration to 'grid-auto-rows: auto' so that the rows will be defined automatically based on the content in the column. This did fix the issue, but it still does not match my design perfectly, as the amount of content in one column changes the row height and affects the other column.</p>

      <p>To create the last section 'What can we do?', I made the element span the full width of the grid using 'grid-column: 1 / span 11;', set a max-width of 800px and then used 'margin:0 auto' to centre the block in the grid.</p>

      <img src="/assets/images/blog/grid/grid-bottom-section.jpg" alt="">

      <p>Making the page responsive using Grid was fairly simple. Inside breakpoints, I redeclared the values for the placement of the child elements to span the full width of the grid container to change the layout into one column.</p>

      <img src="/assets/images/blog/grid/grid-responsive-code.jpg" alt="">


      <h2>Evaluation</h2>
      <p>Despite running into some issues, Grid proved itself to be a powerful CSS layout tool, allowing developers to create more complex layouts quite easily. In this project, I only used a small amount of CSS Grid declarations compared to the exhaustive list shown on CSS Tricks. Using Grid in this project has made me want to explore Grid further and make use of the wide range of declarations available to create more complex layouts in future projects.</p>

      <p>With plenty of useful resources available such as <a href="https://css-tricks.com/snippets/css/complete-guide-grid/" target="_blank">A Complete Guide To Grid</a> and <a href="https://gridbyexample.com/" target="_blank">Grid By Example</a>, I found that I picked CSS Grid up quite quickly. I did run into a few issues as mentioned above, but I am pleased with the outcome of this project and I will be using Grid again in the future. </p>

      <a href="http://saveourplanet.ashleydutton.co.uk" target="_blank">View SaveOurPlanet</a>

      <div class="blog-post__bibliography">
        <h2>Bibliography</h2>
        <p>CSS-Tricks. (2016) <em>A Complete Guide to Grid</em>. [Online] [Accessed on 12th March 2019] <a href="https://css-tricks.com/snippets/css/complete-guide-grid/">https://css-tricks.com/snippets/css/complete-guide-grid/</a></p>

        <p>Andrew, R. (no date) <em>Grid by Example</em>. [Online] [Accessed on 12th March 2019] <a href="https://gridbyexample.com/">https://gridbyexample.com/</a></p>
      </div>

    </div> <!-- post content end -->

    <div class="blog-post__view-more">
      <h2 class="acta acta--title font--25px">View More Posts</h2>
      <ul class="blog__list">
        <li class="blog__post">
          <a href="/blog/the-industry">
            <div class="blog__image" style="background-image:url('/assets/images/blog/the-industry.jpg')"></div>
            <div class="blog__text">
              <p class="karla font--14px font--lightgrey">15th October 2018</p>
              <h2 class="acta font--30px font--white">The UK's Digital Industry</h2>
            </div>
          </a>
        </li>

        <li class="blog__post">
          <a href="/blog/building-my-portfolio">
            <div class="blog__image" style="background-image:url('/assets/images/blog/portfolio.jpg')"></div>
            <div class="blog__text">
              <p class="karla font--14px font--lightgrey">30th October 2018</p>
              <h2 class="acta font--30px font--white">Building My Portfolio</h2>
            </div>
          </a>
        </li>
      </ul>
    </div>

  </section>
</main>

<?php include "../footer.php";?>
