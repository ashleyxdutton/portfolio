<?php
  $page = "blog";
  $title = "Building My Portfolio | Website Design & Development by Ashley Dutton";
  $description = "Blog post talking about the tools and techniques used to design and develop my portfolio.";
  include "../header.php";
?>

<main class="site-content" style="z-index:99;">
  <section class="blog-post">
    <h1 class="acta acta--title">Building My Portfolio</h1>

    <div class="blog-post__meta">
      <img src="/assets/images/user.jpg" alt="Portrait image of myself.">
      <div>
        <p class="karla font--lightgrey">30th October 2018</p>
        <p class="karla font--lightgrey">4 min read time</p>
      </div>
    </div>

    <div class="blog-post__content">
      <p>You are currently viewing the latest iteration of my personal portfolio, and a lot has changed since my previous version. I have learned a whole lot in the past year since my last portfolio, both in terms of design and development. I wanted to bundle all of that new knowledge into a fresh site to provide a solid example of my current skillset.</p>
      <img src="/assets/images/blog/portfolio.jpg" alt="My computer setup with my laptop, speakers and monitor displaying my finished website.">

      <h2>Research</h2>
      <p>Before starting any designing or coding, I sat down to research and plan where I was going to take my portfolio in order to get the most out of it. I spent a lot of time looking around at other peoples portfolios from the industry to get an idea of what to include. The first place I started looking was <a class="link font--black" href="#">Awwwards</a> as these are tried and tested to be well polished, unique websites that stand out among others. I gained a solid foundation in my head of where I wanted to take my website and from there I sat down with a pencil and paper to put some ideas down.</p>

      <h2>Design</h2>
      <p>After putting my ideas down, I opened my design program of choice, Adobe XD, and started working up some design concepts. I had an initial layout in mind and after playing around with a variety of fonts and colours, I was happy that I had something to start working with. I decided to go with a dark UI with large white text and a bright accent colour. As proven in some studies, the greater the contrast between colour combinations, the better the performance (Tubik Studio, 2016).</p>
      <img src="/assets/images/blog/portfolio/concepts.jpg" alt="My inital design concepts on in Adobe XD.">

      <h2>Development</h2>
      <p>I believe that my development skills have come along way in the past year and I wanted to demonstrate this in my portfolio. Back then I had no idea about BEM (Block Element Modifier) methodology, Stacks, SASS, NPM or any of what seemed like wizardry to me a year ago. I started by setting up a local development environment using ScotchBox (a simple Vagrant LAMP stack) and creating my project in <a class="link font--black" href="#">Github</a>.</p>
      <p>To make my workflow a lot more efficient, I set up my folder structure and files to begin working with. I love using SASS when I am developing my sites so I went about setting up Grunt to compile my SASS into a usable CSS file. Using SASS really helps me manage all of my CSS, keeping it broken up into relevant bite-sized chunks making it a lot more maintainable and clear.</p>
      <p>To keep my coding well organised and structured, I used <a class="link font--black" href="#">BEM</a> naming convention and OOCSS (Object Orientated CSS). BEM allows you to increase productivity, by making your code scalable and reusable (Belaya, 2018) and OOCSS encourages stylesheets that are faster and easier to maintain (Lazaris, 2011).</p>
      <blockquote>
        "BEM makes your code scalable and reusable, thus increasing productivity and facilitating teamwork. Even if you are the only member of the team, BEM can be useful for you."<br>- Inna Belaya
      </blockquote>
      <p>I love using OOCSS in particular as it allows me to create my styles based on my designs and then quickly apply them CSS classes as I am coding my markup, rather than rewriting styles over and over again for different elements across the site. An example of how I have used the two together can be found in my landing page section.</p>
      <img src="/assets/images/blog/portfolio/code.jpg" alt="Screenshot of my homepage code open in Atom coding editor.">
      <p>Here I have my section called .hero, then my content block inside hero called .hero__content, using BEM notation to style my hero section. My text inside the hero is then styled using objects that I've previously created to set the font, the size, and the colour to prevent having to write font-family, font-color, and font-size every time I want to style some text. On a larger project, it may not be a good idea to name classes specifically by the font name as I have, as the fonts used may change at a larger date, '.header' and '.bodyfont' may be more appropriate.</p>

      <h2>Future Plans</h2>
      <p>As I continue learning new things, I will continue updating my website to reflect my additional knowledge. My next steps are to learn Greensock, a JavaScript animation library, and Barba.js to add loading animations and page transitions to make my site feel more alive. </p>

      <div class="blog-post__bibliography">
        <h2>Bibliography</h2>
        <p>Belaya. (2018) <em>BEM For Beginners: Why You Need BEM</em>. [Online] [Accessed on 30th October 2018] <a href="https://www.smashingmagazine.com/2018/06/bem-for-beginners/">https://www.smashingmagazine.com/2018/06/bem-for-beginners/</a></p>
        <p>Lazaris. (2011) <em>An Introduction To Object Oriented CSS (OOCSS)</em>. [Online] [Accessed on 30th October 2018] <a href="https://www.smashingmagazine.com/2011/12/an-introduction-to-object-oriented-css-oocss/">https://www.smashingmagazine.com/2011/12/an-introduction-to-object-oriented-css-oocss/</a></p>
        <p>Tubik Studio. (2016) <em>Dark Side of UI. Benefits of Dark Background</em>. [Online] [Accessed on 30th October 2018] <a href="https://uxplanet.org/dark-side-of-ui-benefits-of-dark-background-12f560bf7165">https://uxplanet.org/dark-side-of-ui-benefits-of-dark-background-12f560bf7165</a></p>
      </div>

    </div> <!-- post content end -->

    <div class="blog-post__view-more">
      <h2 class="acta acta--title font--25px">View More Posts</h2>
      <ul class="blog__list">

        <li class="blog__post">
          <a href="/blog/the-industry">
            <div class="blog__image" style="background-image:url('/assets/images/blog/the-industry.jpg')"></div>
            <div class="blog__text">
              <p class="karla font--14px font--lightgrey">15th October 2018</p>
              <h2 class="acta font--30px font--white">The UK's Digital Industry</h2>
            </div>
          </a>
        </li>

        <li class="blog__post">
          <a href="/blog/performance-and-optimisation">
            <div class="blog__image" style="background-image:url('/assets/images/blog/performance.jpg')"></div>
            <div class="blog__text">
              <p class="karla font--14px font--lightgrey">8th November 2018</p>
              <h2 class="acta font--30px font--white">Performance and Optimisation</h2>
            </div>
          </a>
        </li>

      </ul>
    </div>

  </section>
</main>

<?php include "../footer.php";?>
