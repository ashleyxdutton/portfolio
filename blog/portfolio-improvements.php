<?php
  $page = "blog";
  $title = "Portfolio Improvements | Website Design & Development by Ashley Dutton";
  $description = "Blog post talking about the tools and techniques used to design and develop my portfolio.";
  include "../header.php";
?>

<main class="site-content" style="z-index:99;">
  <section class="blog-post">
    <h1 class="acta acta--title">Portfolio Improvements</h1>

    <div class="blog-post__meta">
      <img src="/assets/images/user.jpg" alt="Portrait image of myself.">
      <div>
        <p class="karla font--lightgrey">26th March 2019</p>
        <p class="karla font--lightgrey">2 min read time</p>
      </div>
    </div>

    <div class="blog-post__content">

      <p>Since completing the first version of my website back in November, I have made a few changes to improve the overall experience. The changes that have been made are mostly focused on performance enhancements and usability fixes.</p>

      <img src="/assets/images/blog/improvements.jpg" alt="iPhone X with my website open.">

      <h2>Usability</h2>
      <p>I first looked at how the usability of my website could be improved. Whilst reviewing my website, I discovered that at the end of my project posts and blog posts, there was nowhere for the user to go, as I had no 'top' button and no links for the user to click, meaning they would have to scroll all the way back to the top. This was particularly an issue for blog posts, as these could get quite lengthy and would mean a lot of scrolling to return to the top. To overcome this issue, I implemented a 'View More Posts' section with links to more of my blog posts, to encourage the user to continue reading more of my posts.</p>

      <img src="/assets/images/blog/improvements-blog.jpg" alt="New View More Blog posts section on my website.">

      <h2>Performance Enhancements</h2>
      <p>I also made some performance enhancements to my website. My CSS was already minified using the grunt-contrib-cssmin task with Grunt but my JavaScript was not. I found a highly useful package within Atom called atom-minify that allowed me to minify my JavaScript files to a new minified file each time that I saved. This meant I could keep an editable, non-minified version of my scripts to edit whilst having a minified version for the production version of my website.</p>
      <p>To further enhance the speed and security of my website, I used <a href="https://www.cloudflare.com/" target="_blank">CloudFlare</a>. Cloudflare provides a content delivery network (CDN) service to optimise the delivery of webpages to get faster loading times. To do this, I created an account on Cloudflare and then updated my domain nameservers in 34SP to the ones provided by Cloudflare. This is super simple to do yet provides a lot of benefits, such as automatically applying optimisations to webpages. Cloudflare works by sitting between the visitor and the website's hosting provider and acts as a reverse proxy.</p>
      <p>To improve accessibility, I also added titles to the SVGs that I had used throughout the site. The title tag provides a short description of the SVG, improving accessibility for impaired users.</p>

      <h2>Future Plans</h2>
      <p>I intend to write more blogs posts in the near future, but as my project is currently static, I have to manually create a HTML file every time. I am going to look at either putting my website into a content management system like WordPress or a static site generator like Jekyll so that I can create new blog posts from markdown files.</p>
      <p>I would also like to make improvements to my project pages to include more in-depth information about each project that I have completed. If I decide to use a content management system, I would also like to create a page builder type setup to quickly put case studies together.</p>

    </div> <!-- post content end -->

    <div class="blog-post__view-more">
      <h2 class="acta acta--title font--25px">View More Posts</h2>
      <ul class="blog__list">
        <li class="blog__post">
          <a href="/blog/the-industry">
            <div class="blog__image" style="background-image:url('/assets/images/blog/the-industry.jpg')"></div>
            <div class="blog__text">
              <p class="karla font--14px font--lightgrey">15th October 2018</p>
              <h2 class="acta font--30px font--white">The UK's Digital Industry</h2>
            </div>
          </a>
        </li>

        <li class="blog__post">
          <a href="/blog/building-my-portfolio">
            <div class="blog__image" style="background-image:url('/assets/images/blog/portfolio.jpg')"></div>
            <div class="blog__text">
              <p class="karla font--14px font--lightgrey">30th October 2018</p>
              <h2 class="acta font--30px font--white">Building My Portfolio</h2>
            </div>
          </a>
        </li>
      </ul>
    </div>

  </section>
</main>

<?php include "../footer.php";?>
