<?php
  $page = "home";
  $title = "Website Design & Development by Ashley Dutton";
  $description = "I am a Manchester-based website designer and developer with plenty of experience, as well as branding and WordPress. I have worked with a range of clients, both small and large.";
  include "header.php";
?>

<main class="site-content">

  <section class="hero">
    <div class="hero__content">
      <h1 class="acta font--50px font--white">I am Ashley Dutton, a digital designer and front-end developer</h1>
      <p class="karla font--18px font--lightgrey">I am a Manchester-based website designer and developer with plenty of experience, as well as branding and WordPress. I have worked with a range of clients, both small and large. I also work as a front-end developer at digital agency Supremo. </p>

      <a class="button" href="/projects.php">
        <span>View Projects</span>
        <?php include "assets/svgs/arrow.svg";?>
      </a>

      <a class="karla karla--bold font--white link" href="/contact" data-tilt>Get in touch</a>
    </div>

    <div class="hero__socials">
      <ul>
        <li><a class="hero__social" href="https://twitter.com/ashleyxdutton" target="_blank"><?php include "assets/svgs/twitter.svg";?></a></li>
        <li><a class="hero__social" href="https://dribbble.com/ashleyxdutton" target="_blank"><?php include "assets/svgs/dribbble.svg";?></a></li>
        <li><a class="hero__social" href="https://gitlab.com/ashleyxdutton" target="_blank"><?php include "assets/svgs/gitlab.svg";?></a></li>
      </ul>
    </div>

  </section>
</main>

<?php
  include "footer.php";
?>
