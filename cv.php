<?php
  $page = "about";
  $title = "Curriculum Vitae | Website Design & Development by Ashley Dutton";
  $description = "Read my curriculum vitae, containing my past work experience and my education.";
  include "header.php";
?>

<main class="site-content">
  <section class="cv">
    <h1 class="acta acta--title">Curriculum Vitae</h1>

    <div class="container">
      <div class="cv__left">
        <div class="cv__section">
          <h2 class="karla karla--bold font--white">Contact Details</h2>
          <p class="karla font--lightgrey">07500899566</p>
          <p class="karla font--lightgrey">hello@ashleydutton.co.uk</p>
          <p class="karla font--lightgrey">Manchester, UK</p>
        </div>

        <div class="cv__section">
          <h2 class="karla karla--bold font--white">Skills</h2>
          <p class="karla font--lightgrey">Adobe CC (Full Package)</p>
          <p class="karla font--lightgrey">HTML</p>
          <p class="karla font--lightgrey">CSS</p>
          <p class="karla font--lightgrey">JavaScript</p>
          <p class="karla font--lightgrey">WordPress</p>
          <p class="karla font--lightgrey">SASS</p>
          <p class="karla font--lightgrey">Grunt</p>
          <p class="karla font--lightgrey">NPM</p>
          <p class="karla font--lightgrey">Git</p>
        </div>

      </div>

      <div class="cv__right">
        <div class="cv__section">
          <h2 class="karla karla--bold font--white">Profile</h2>
          <p class="karla font--lightgrey">I am Ashley Dutton, I'm currently studying Web Development at Manchester Metropolitan University and working as a front-end developer at a Manchester-based digital agency, Supremo. I am a highly motivated and creative person and I am always looking to make my next project better than my last. I have worked freelance with a range of clients and have worked on some major projects during my time with Supremo. I have a keen eye for design and pay close attention to the smallest of details which make up for a better overall product. I also strive to stay up-to-date with the latest development methods and apply them to my work where necessary.</p>
        </div>

        <div class="cv__section">
          <h2 class="karla karla--bold font--white">Work Experience</h2>
          <p class="karla font--lightgrey">(2017 - Present) Supremo — Front-End Developer.</p>
          <p class="karla font--lightgrey">(2016 - Present) Freelance Web Designer & Developer.</p>
          <p class="karla font--lightgrey">(2015 - 2016) Ben Sherman — Sales Assistant.</p>
        </div>

        <div class="cv__section">
          <h2 class="karla karla--bold font--white">Education</h2>
          <p class="karla font--lightgrey">(2016 - Present) Manchester Metropolitan University</p>
          <p class="karla font--white">BSc Web Development</p>
          <p class="karla font--lightgrey">(2014 - 2016) West Cheshire College</p>
          <p class="karla font--white">Level 3 Software Development — DMM</p>
          <p class="karla font--lightgrey">(2011 - 2013) UCE Academy</p>
          <p class="karla font--white">8 GCSEs (C - A*)</p>
        </div>
      </div>
    </div>
  </section>
</main>

<?php include "footer.php";?>
