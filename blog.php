<?php
  $page = "blog";
  $title = "Blog Posts | Website Design & Development by Ashley Dutton";
  $description = "Read some of my latest blog posts discussing the digital industry, projects that I have been working on and general thoughts.";
  include "header.php";
?>

<main class="site-content">
  <section class="blog">
    <h1 class="acta acta--title">Blog Posts</h1>

    <div class="container container--blog">

      <ul class="blog__list">
        <li class="blog__post" data-tilt>
          <a href="/blog/introduction-to-greensock">
            <div class="blog__image" style="background-image:url('assets/images/blog/greensock.jpg')"></div>
            <div class="blog__text">
              <p class="karla font--14px font--lightgrey">26th March 2019</p>
              <h2 class="acta font--30px font--white">An introduction to GreenSock</h2>
            </div>
          </a>
        </li>

        <li class="blog__post" data-tilt>
          <a href="/blog/layouts-using-css-grid">
            <div class="blog__image" style="background-image:url('assets/images/blog/css-grid.jpg')"></div>
            <div class="blog__text">
              <p class="karla font--14px font--lightgrey">26th March 2019</p>
              <h2 class="acta font--30px font--white">Layouts using CSS Grid</h2>
            </div>
          </a>
        </li>

        <li class="blog__post" data-tilt>
          <a href="/blog/portfolio-improvements">
            <div class="blog__image" style="background-image:url('assets/images/blog/improvements.jpg')"></div>
            <div class="blog__text">
              <p class="karla font--14px font--lightgrey">26th March 2019</p>
              <h2 class="acta font--30px font--white">Portfolio Improvements</h2>
            </div>
          </a>
        </li>

        <li class="blog__post" data-tilt>
          <a href="/blog/performance-and-optimisation">
            <div class="blog__image" style="background-image:url('assets/images/blog/performance.jpg')"></div>
            <div class="blog__text">
              <p class="karla font--14px font--lightgrey">8th November 2018</p>
              <h2 class="acta font--30px font--white">Performance and Optimisation</h2>
            </div>
          </a>
        </li>

        <li class="blog__post" data-tilt>
          <a href="/blog/building-my-portfolio">
            <div class="blog__image" style="background-image:url('assets/images/blog/portfolio.jpg')"></div>
            <div class="blog__text">
              <p class="karla font--14px font--lightgrey">30th October 2018</p>
              <h2 class="acta font--30px font--white">Building My Portfolio</h2>
            </div>
          </a>
        </li>

        <li class="blog__post" data-tilt>
          <a href="/blog/the-industry">
            <div class="blog__image" style="background-image:url('assets/images/blog/the-industry.jpg')"></div>
            <div class="blog__text">
              <p class="karla font--14px font--lightgrey">15th October 2018</p>
              <h2 class="acta font--30px font--white">The UK's Digital Industry</h2>
            </div>
          </a>
        </li>
      </ul>
    </div>
  </section>
</main>

<?php
  include "footer.php";
?>
