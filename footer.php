  <div class="background background--static"></div>
  <div class="background background--topographic"></div>
  <div class="background__border"></div>

  <footer class="site-footer">
    <div class="site-footer__top">
      <h3 class="karla font--white">Let’s work together.</h3>
      <a class="acta font--35px font--white link" href="mailto:hello@ashleydutton.co.uk">hello@ashleydutton.co.uk</a>
    </div>

    <div class="site-footer__bottom">
      <p class="karla font--lightgrey">© Ashley Dutton 2019</p>
      <ul>
        <li><a class="karla font--lightgrey" href="https://twitter.com/ashleyxdutton">Twitter</a></li>
        <li><a class="karla font--lightgrey" href="https://dribbble.com/ashleyxdutton">Dribbble</a></li>
        <li><a class="karla font--lightgrey" href="https://gitlab.com/ashleyxdutton">Gitlab</a></li>
      </ul>
    </div>
  </footer>

  <script src="/assets/js/jquery.min.js" defer></script>
  <script src="/assets/js/tilt.jquery.js" defer></script>
  <script src="/assets/js/scripts.min.js" defer></script>

</body>

</html>
