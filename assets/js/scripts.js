function navigation() {
  const navBtn = document.querySelector('.site-header__nav-btn');
  const navIcon = document.querySelector('.site-header__nav-icon');
  const navBg = document.querySelector('.site-header__nav-bg');
  const nav = document.querySelector('.site-header__nav');

  navBtn.addEventListener("click", function() {
    navBg.classList.toggle('open');
    navIcon.classList.toggle('open');
    nav.classList.toggle('open');
  });
}
navigation();
 
/*--------------------------------------*/

// Tilt Settings

function tilt() {
  $('.projects__item, .blog__post, .button').tilt({
    perspective: 3000,
    scale: 1.04
  })
}
tilt();
