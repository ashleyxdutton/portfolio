<!DOCTYPE html>
<html lang="en">
<head>

	<!-- metadata -->
	<meta charset="UTF-8">
	<title><?php echo $title;?></title>
  <meta name="description" content="<?php echo $description;?>">
	<meta name="keywords" content="website, design, development, student, freelance, html, css, ui">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">

	<!-- stylesheets -->
	<link rel="stylesheet" type="text/css" href="/assets/css/main.css">

	<!-- fonts -->
	<link href="https://fonts.googleapis.com/css?family=Karla:400,700" rel="stylesheet">
	<link rel="stylesheet" href="https://use.typekit.net/akn3ptn.css">

	<!-- favicon -->
	<link rel="apple-touch-icon" sizes="57x57" href="/assets/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/assets/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/assets/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/assets/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/assets/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/assets/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/assets/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/assets/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/assets/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/assets/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon/favicon-16x16.png">
	<link rel="manifest" href="/assets/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/assets/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<!-- Google Analytics Tracking -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-64321706-6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-64321706-6');
</script>


</head>
<body>

<header class="site-header">

	<a class="site-header__logo karla karla--bold font--30px font--white" href="/">Ad</a>

	<nav class="site-header__nav">

		<ul class="site-header__nav-list">

			<li class="site-header__item">
				<a class="site-header__link karla font--lightgrey font--14px <?php if($page=='home'){echo 'active';}?>" href="/">Home</a>
			</li>

			<li class="site-header__item">
				<a class="site-header__link karla font--lightgrey font--14px <?php if($page=='about'){echo 'active';}?>" href="/about">About</a>
			</li>

			<li class="site-header__item">
				<a class="site-header__link karla font--lightgrey font--14px <?php if($page=='projects'){echo 'active';}?>" href="/projects.php">Projects</a>
			</li>

			<li class="site-header__item">
				<a class="site-header__link karla font--lightgrey font--14px <?php if($page=='blog'){echo 'active';}?>" href="/blog.php">Blog</a>
			</li>

			<li class="site-header__item">
				<a class="site-header__link karla font--lightgrey font--14px <?php if($page=='contact'){echo 'active';}?>" href="/contact">Contact</a>
			</li>

		</ul>
	</nav>

	<div class="site-header__nav-btn-wrapper">
		<div class="site-header__nav-bg"></div>
		<a class="site-header__nav-btn" href="#">
			<span class="site-header__nav-icon"></span>
		</a>
	</div>

</header>
