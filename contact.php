<?php
  $page = "contact";
  $title = "Contact Me | Website Design & Development by Ashley Dutton";
  $description = "Get in touch with me using the form or my email to discuss my work or your project.";
  include "header.php";
?>

<main class="site-content">
  <section class="contact">
    <h1 class="acta acta--title">Contact</h1>

    <div class="container container--blog">

      <form class="contact__form" action="https://formspree.io/hello@ashleydutton.co.uk" method="post">
        <fieldset>
          <label class="karla font--lightgrey" for="name">Your name* </label>
          <input class="karla" type="text" name="Name" id="name" required>
        </fieldset>

        <fieldset>
          <label class="karla font--lightgrey" for="message">Your message* </label>
          <textarea class="karla" name="Message" rows="8" cols="80" id="message" required></textarea>
        </fieldset>

        <fieldset>
          <label class="karla font--lightgrey" for="email">Your email* </label>
          <input class="karla" type="email" name="Email" id="email" required>
        </fieldset>

        <input class="button" type="submit" name="Submit" value="Send">
      </form>

      <div class="contact__text">
        <h2 class="karla font--25px font--white">Want to work with me?</h2>
        <p class="karla font--lightgrey">Let’s talk about your project! Use the form to get in touch or drop me an email at <a class="karla--bold font--white link" href="mailto:hello@ashleydutton.co.uk">hello@ashleydutton.co.uk.</a></p>
        <ul>
          <li><a class="hero__social" href="https://twitter.com/ashleyxdutton"><?php include "assets/svgs/twitter.svg";?></a></li>
          <li><a class="hero__social" href="https://dribbble.com/ashleyxdutton"><?php include "assets/svgs/dribbble.svg";?></a></li>
          <li><a class="hero__social" href="https://gitlab.com/ashleyxdutton"><?php include "assets/svgs/gitlab.svg";?></a></li>
        </ul>
      </div>

    </div>
  </section>
</main>

<?php include "footer.php";?>
